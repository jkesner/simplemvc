-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 23. lis 2016, 15:26
-- Verze serveru: 5.7.14
-- Verze PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `simplemvc_03`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `novinky`
--

CREATE TABLE `novinky` (
  `novinky_id` int(11) NOT NULL,
  `novinky_nadpis` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `novinky_text` longtext COLLATE utf8_czech_ci NOT NULL,
  `novinky_datum` datetime NOT NULL,
  `novinky_jazyk` varchar(2) COLLATE utf8_czech_ci NOT NULL,
  `novinky_html_nazev` text COLLATE utf8_czech_ci NOT NULL,
  `novinky_titulek` text COLLATE utf8_czech_ci NOT NULL,
  `novinky_description` text COLLATE utf8_czech_ci NOT NULL,
  `novinky_keywords` text COLLATE utf8_czech_ci NOT NULL,
  `novinky_publikovat` smallint(2) NOT NULL,
  `novinky_typ` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `novinky`
--

INSERT INTO `novinky` (`novinky_id`, `novinky_nadpis`, `novinky_text`, `novinky_datum`, `novinky_jazyk`, `novinky_html_nazev`, `novinky_titulek`, `novinky_description`, `novinky_keywords`, `novinky_publikovat`, `novinky_typ`) VALUES
(35, 'commit #881dfd2', '<p><a href="https://bitbucket.org/jkesner/simplemvc/commits/881dfd2962a59d1afd2d937c6909222ce4b7dc97?at=master" target="_blank">new commit improves core classes & style</p>', '2015-05-15 14:56:00', 'cz', '', '', '', '', 1, 0),
(37, 'commit #14aa2ff', 'adds MeekroDB library, bugfixes', '2016-11-23 00:00:00', 'cz', 'commit-14aa2ff', 'commit #14aa2ff', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `sekce`
--

CREATE TABLE `sekce` (
  `sekce_id` int(11) NOT NULL,
  `sekce_nadrazena_id` int(11) NOT NULL,
  `sekce_html_nazev` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `sekce_nazev` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `sekce_poradi` int(11) NOT NULL,
  `sekce_obsah` longtext CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `sekce_titulek` text NOT NULL,
  `sekce_description` text NOT NULL,
  `sekce_keywords` text NOT NULL,
  `sekce_jazyk` varchar(2) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `sekce_publikovat` smallint(2) NOT NULL,
  `sekce_in_sitemap` smallint(2) NOT NULL,
  `sekce_level` smallint(2) NOT NULL,
  `sekce_users_skupina` int(11) NOT NULL,
  `sekce_datum_zmeny` datetime NOT NULL,
  `sekce_frekvence_zmen` varchar(40) NOT NULL,
  `sekce_galerie` smallint(2) NOT NULL,
  `sekce_type` varchar(20) NOT NULL,
  `sekce_image` varchar(50) NOT NULL,
  `sekce_free_3` smallint(1) NOT NULL,
  `menuLevel` int(1) NOT NULL,
  `sekce_modul` char(1) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `sekce`
--

INSERT INTO `sekce` (`sekce_id`, `sekce_nadrazena_id`, `sekce_html_nazev`, `sekce_nazev`, `sekce_poradi`, `sekce_obsah`, `sekce_titulek`, `sekce_description`, `sekce_keywords`, `sekce_jazyk`, `sekce_publikovat`, `sekce_in_sitemap`, `sekce_level`, `sekce_users_skupina`, `sekce_datum_zmeny`, `sekce_frekvence_zmen`, `sekce_galerie`, `sekce_type`, `sekce_image`, `sekce_free_3`, `menuLevel`, `sekce_modul`) VALUES
(71, 0, 'magna-aliqua', 'Magna aliqua', 2, '<h2>lorem ipsum</h2>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n</p>', '', '', '', 'cz', 1, 1, 1, 0, '2015-07-01 09:51:00', 'yearly', 0, '0', '0', 0, 0, 's'),
(72, 0, 'reprehenderit', 'Reprehenderit', 5, '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n</p>', '', '', '', 'cz', 1, 1, 1, 0, '2015-06-29 11:39:00', 'yearly', 0, 'kontakt', '0', 0, 0, 's'),
(103, 76, 'subpage-et-lorem', 'Subpage et lorem', 0, '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<p>\r\nView class has also implemented access to core as plugin, so templates has access to call functions directly from themes\r\n</p>', '', '', '', 'cz', 1, 1, 2, 0, '2015-08-07 00:00:00', 'yearly', 0, '0', '0', 0, 0, 's'),
(76, 0, 'delor-ist', 'Delor-ist', 3, '<h2>lorem ipsum</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> ', '', '', '', 'cz', 1, 1, 1, 0, '2015-07-03 12:46:00', 'yearly', 0, '0', '0', 0, 0, 's');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `novinky`
--
ALTER TABLE `novinky`
  ADD PRIMARY KEY (`novinky_id`);

--
-- Klíče pro tabulku `sekce`
--
ALTER TABLE `sekce`
  ADD PRIMARY KEY (`sekce_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `novinky`
--
ALTER TABLE `novinky`
  MODIFY `novinky_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pro tabulku `sekce`
--
ALTER TABLE `sekce`
  MODIFY `sekce_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
