<section class="content services container container_12 t_container_8 m_container_4 center">
	<div class="editorContent grid_8 t_grid_5 m_grid_4">
		{if $homepage.0 != "" && $homepage.0.sekce_obsah}
			{$homepage.0.sekce_obsah}
		{else}
			&nbsp;	
		{/if}
	</div>
	<div class="sidebar news grid_4 t_grid_3 m_grid_4">
		<div class="padd">
			<h3>News</h3>
			<ul class="newsList">
				{foreach $news n}
					<li>
						<p>
							<strong>{$n.novinky_nadpis}</strong><br>
							<time datetime="{$n.novinky_datum}">
								{load tools format_date $n.novinky_datum DATE_ONLY}
							</time><br>
							{load tools striphtml $n.novinky_text}
						</p>
					</li>
				{else}
					<li><p>No news found.</p></li>
				{/foreach}
			</ul>
		</div>
	</div>
</section>