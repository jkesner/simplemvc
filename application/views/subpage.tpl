<section class="content subpage container container_12 t_container_8 m_container_4 center">
	<div class="editorContent grid_12 t_grid_8 m_grid_4">
		{if $data != ""}
			<h1>{$data.sekce_nazev}</h1>
			{$data.sekce_obsah}
		{else}
			<h2>Stránka nebyla nalezena</h2>
			{*load tools redirect*}
		{/if}
	</div>
</section>