<!DOCTYPE html>
<html lang="cs">
	<head>
		<meta charset="UTF-8">
		<title>{$title}</title>
		<base href="{$core.config.base_url}">
		<link href="static/css/style.less" type="text/less" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="static/js/head.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript">
			head.js( 
		        { less: "static/js/less-1.4.1.min.js" }, 
		        { jQuery: "static/js/jquery-1.11.3.min.js" }, 
		        { bootStrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" }
			);
		</script>
	</head>
	<body>
		<header class="container container_12 t_container_8 m_container_4 center">
			<nav class="navbar navbar-default main grid_12 t_grid_8 m_grid_4">
				<div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		              	<span class="sr-only">Toggle navigation</span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="{$core.config.base_url}#">the title</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
	            	<ul class="nav navbar-nav">
						{foreach $nav item}
							<li class="level_1">
								{if $item.submenu != false && $item.hideSubmenu != 1}
									<a href="#" title="{$item.name}" class="dropdown-toggle" data-toggle="dropdown" role="button" 
										aria-haspopup="true" aria-expanded="false">
										{$item.name} <span class="caret"></span>
									</a>
									<ul class="dropdown-menu submenu level_2">
										{foreach $item.submenu sm}
											<li class="level_2 fourth">
												<a href="{$sm.url}" title="{$sm.name} - {$item.name}" class="level_2">
													{$sm.name}
												</a>
											</li>
										{/foreach}
									</ul>
								{else}
									<a href="{$item.url}" title="{$item.name}">{$item.name}</a>
								{/if}
							</li>
						{/foreach}
					</ul>
				</div>
			</nav>
		</header>

		{$content}
		
		<footer class="container container_12 t_container_8 m_container_4 center">
			<div class="grid_6 t_grid_4 m_grid_4">
				lorem ipsum delor ist amet, this is fork of <a href="https://github.com/gilbitron/PIP" target="_blank" title="fork of pip">pip</a>, with improvements:<br> 
				<a href="http://dwoo.org" target="_blank" title="DWOO template engine">DWOO template engine</a>, <a href="http://lesscss.org/" target="_blank">LESSCSS</a>, <a href="http://jquery.com" target="_blank">jQuery</a>,  
				<a href="http://getbootstrap.com/" target="_blank" title="bootstrap v3">bootstrap v3</a> & <a href="http://960.gs/" target="_blank" title="960.gs">960 grid system</a>, <a href="http://meekro.com/" target="_blank">MeekroDB PHP MYSQL library</a>
			</div>
			<div class="grid_6 t_grid_4 m_grid_4">
				Memory usage: {load tools getMemoryUsage}<br>
				Process time: {load tools getTimeUsage}
			</div>
		</footer>

		<div class="debug container container_12 t_container_8 m_container_4 center">
			<div class="grid_12 t_grid_8 m_grid_4">
				<strong>Debug:</strong><br>
				{$core.debug}
			</div>
		</div>
	</body>
</html>