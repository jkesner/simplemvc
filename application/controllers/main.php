<?php

class Main extends Controller {
	
	public function render ( $template_data, $data_modify = false ) {
		$model = $this->loadModel( "MainModel" );
		$data = array(
			"title" => $model->getTitle(),
			"nav" => $model->getNav()
		);
		if ( is_array( $data_modify ) ) {
			$data = array_merge( $data, $data_modify );
		}
		$data["content"] = $template_data;
		$template = $this->loadView();
		return $template->render( "layout.tpl", $data );
	}

	function index () {
		$this->core->tools->debug( "MainController->index()" );
		$model = $this->loadModel( "MainModel" );
		$template = $this->loadView();
		$homepage = $model->getHomepage();
		$news = $model->getNews();
		$content = $template->render( 
			"homepage.tpl", 
			array( 
				"news" => $news, 
				"homepage" => $homepage 
			) 
		);
		return $this->render( $content );
	}
}

?>
