<?php

class subpage extends Controller {
	
	function handler ( $p1 = false, $p2 = false, $p3 = false ) {
		$this->core->tools->debug( "SubpageController->handler()" );
		$model = $this->loadModel( "SubpageModel" );
		$template = $this->loadView();
		if ( $id = $model->exists( $p1, $p2, $p3 ) ) {
			$content = $model->getData( $id );
    		$subpage = $template->render( 
    			"subpage.tpl", 
    			array(
    				"data" => $content, 
    			) 
    		);
			return $this->core->main->render( $subpage );
		} 
		$error = $template->render("content.tpl", array("content" => $this->core->tools->msg("Chyba stránka nebyla nalezena", "error")));
		return $this->core->main->render( $error );
	}
	
	function index () {
		$this->core->tools->debug("SubpageController->index()");
		$template = $this->loadView();
		$data = $template->render( "subpage index", "homepage.tpl" );
		return $this->core->main->render( $data );
	}


	
   
}

?>
