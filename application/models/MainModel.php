<?php

class MainModel extends Model {
	
	public function getNav ( $parentId = 0 ) {
		$out = array();
		$data = DB::query("select * from sekce where sekce_nadrazena_id = %i order by sekce_poradi asc", $parentId);
		if (is_array($data)){
			foreach ( $data as $item ) {
				$out[] = array( 
					"url" => $this->getUrl( $item["sekce_id"] ), 
					"name" => $item["sekce_nazev"], 
					"submenu" => $this->getNav( $item["sekce_id"] )
				);
			}
		}
		return $out;
	}

	public function getUrl ( $id ) {
		$data = DB::query( "select * from sekce where sekce_id = %i limit 1", $id );
		if ( count( $data ) ) {
			$url = array();
			$parent = $data[0]["sekce_nadrazena_id"];
			$url[] = $data[0]["sekce_html_nazev"];
			while ( $parent != 0 ) {
				$data = DB::query( "select * from sekce where sekce_id = %i", $data[0]["sekce_nadrazena_id"] );
				if ( count( $data ) ) {
					$url[] = $data[0]["sekce_html_nazev"];
					$parent = $data[0]["sekce_nadrazena_id"];
				} else {
					$parent = 0;
				}
			}
			$url = array_reverse($url); 
			return implode( "/", $url );
		}
		return "";
	}

	public function getHomepage (  ) {
		$out = false;
		$data = DB::query("select * from sekce where sekce_html_nazev = %s", "magna-aliqua");
		if (count($data)){
			$out = $data;
		}
		return $out;
	}
	
	public function getNews (  ) {
		$out = false;
		$data = DB::query("select * from novinky order by novinky_datum desc limit 3");
		if (count($data)){
			$out = $data;
		}
		return $out;
	}
	
	public function getTitle () {
		return "the title";
	}

}

?>
