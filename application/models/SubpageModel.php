<?php

class SubpageModel extends Model {
	
	public $title = array();	
	
	/*
		check subpage existency
	*/
	public function exists ( $p1, $p2 = false, $p3 = false ) {
		$this->core->tools->debug("SubpageModel->exists($p1, $p2, $p3)");
		if ( $p1 && $p2 && $p3 ) { 
			$this->core->tools->debug("p1:{$p1} p2:{$p2} p3:{$p3}");
			$data = DB::query( "select * from sekce where sekce_html_nazev = %s order by sekce_poradi desc", $p1 );
			if ( count($data) ) {
				$this->title[] = $data[0]["sekce_nazev"];	
				$data = DB::query( "select * from sekce where sekce_html_nazev = %s and sekce_nadrazena_id = %i order by sekce_poradi desc", $p2, $data[0]["sekce_id"] );
				if ( count( $data ) ) {
					$this->title[] = $data[0]["sekce_nazev"];	
					$data = DB::query( "select * from sekce where sekce_html_nazev = %s and sekce_nadrazena_id = %i", $p3, $data[0]["sekce_id"] );					
					if ( count( $data ) ) {
						$this->title[] = $data[0]["sekce_nazev"];	
						return $data[0]["sekce_id"];
					}
				}
			}
			return false;
		} else if ( $p1 && $p2 ) { 
			if ( $data = DB::query( "select * from sekce where sekce_html_nazev = %s order by sekce_poradi desc", $p1 ) ) {
				$this->title[] = $data[0]["sekce_nazev"];	
				$id = $data[0]["sekce_id"];
				$data = DB::query( 
					"select * from sekce where sekce_html_nazev = %s and sekce_nadrazena_id = %i order by sekce_poradi desc", 
					$p2, 
					$id 
				);
				if ( count( $data ) ) {
					$this->title[] = $data[0]["sekce_nazev"];	
					return $data[0]["sekce_id"];
				}	
			}
			return false; 
		} else if ( $p1 ) { 
			$data = DB::query( "select * from sekce where sekce_html_nazev = %s order by sekce_poradi desc", $p1 );
			if ( count( $data ) ) {
				$this->title[] = $data[0]["sekce_nazev"];	
				return $data[0]["sekce_id"];
			}
		}
		return false;
	}

	
	
	public function getReferences (  ) {
		return "@todo";
		/*$data = $this->core->db->select( "*" )
			->from( "sekce" )
			->join( "sekce_files", "sekce_files.sekce_files_sekce_id<->sekce.sekce_id" )
			->where( array( "sekce_nazev" => "Reference" ) )
			->order( "sekce_poradi", "desc" )
			->rows();
		// die(var_dump($data));
		$i = 0;
		$out  = array();
		foreach ($data as $item){
			$out[$i] = $item;
			$out[$i]["url"] = $this->getUrl($item["sekce_id"]);
			$i++;
		}
		return $out;*/
	}	

	/*public function getNav ( $parentId = 0 ) {
		$data = $this->core->db->select( "sekce_id,sekce_html_nazev,sekce_nazev,sekce_free_3" )
			->from( "sekce" )
			->where( array( "sekce_nadrazena_id" => $parentId ) )
			->order( "sekce_poradi", "asc" )
			->rows();
		if ( !count( $data ) ) { return false; }
		$out = array();
		foreach ( $data as $item ) {
			$out[] = array( 
				"url" => $this->getUrl( $item["sekce_id"] ), 
				"name" => $item["sekce_nazev"], 
				"submenu" => $this->getNav( $item["sekce_id"]), 
				"hideSubmenu" => $item["sekce_free_3"]
			);
		}
		return $out;
	}*/

	/*public function getParentNav ( $parentId = 0 ) {
		$data = $this->core->db->select( "sekce_id,sekce_html_nazev,sekce_nazev,sekce_free_3" )
			->from( "sekce" )
			->where( array( "sekce_nadrazena_id" => $parentId ) )
			->order( "sekce_poradi", "asc" )
			->rows();
		if ( !count( $data ) ) { return false; }
		$out = array();
		foreach ( $data as $item ) {
			$out[] = array( 
				"url" => $this->getUrl( $item["sekce_id"] ), 
				"name" => $item["sekce_nazev"], 
				//"submenu" => $this->getNav( $item["sekce_id"]), 
				"hideSubmenu" => $item["sekce_free_3"]
			);
		}
		return $out;
	}*/


	public function getData ( $id ) {
		if ( $data = DB::query( "select * from sekce where sekce_id = %i", $id ) ) {
			return $data[0];
		}
	}
	
	public function getTitle () {
		return implode(" - ", array_reverse($this->title))." | the seo suffix";
	}

}

?>
