#simpleMVC

simpleMVC code showcase - forked from [http://gilbitron.github.com/PIP](PIP)

improvements are:
• file structure
• autoload
• LESS CSS
• DWOO template engine
• Cross-class instance referencing
• 960gs CSS grid system
• SEO friendly urls
• Bootstrap v3
• MeekroDB PHP library
• & more...

## Requirements

* PHP 5.1 or greater
* MySQL 4.1.2 or greater
* The mod_rewrite Apache module
