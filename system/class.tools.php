<?php
class tools {
	public $dbg = array();
	////////////////////////////////////////////////////////////////////
	public function __construct ( &$core ) {
		$this->core = $core;
		$this->debug("Tools initialized");
		if ( $this->get("bd") ) { return $this->bd(); }
	}	
	////////////////////////////////////////////////////////////////////
	public function sanitize ( $input, $config = false ) {
		return htmlspecialchars( addslashes( $input ) );
	}
	////////////////////////////////////////////////////////////////////
	public function redir ( $loc = "", $delay = 0 ) { return $this->redirect( $loc, $delay ); }
	public function redirect ( $loc = "", $delay = 0 ) {
		//sleep( (int)$delay );
		if ( $loc == "home" ) {
			$loc = $this->core->config['base_url'];
		} else {
			$loc = $this->core->config['base_url'].$loc;
		}
		return '<meta http-equiv="refresh" content="'.$delay.';url='.$loc.'">';
		// header( "refresh:{$delay}; url={$loc}" ); 
		// header('Location: '. $loc);
		// exit;
	}
	////////////////////////////////////////////////////////////////////
	public function remote_redirect ( $loc = "", $delay = 0 ) {
		sleep( (int)$delay );
		header('Location: ' . $loc);
		exit;
	}
	////////////////////////////////////////////////////////////////////
	// info | success | warning | danger
	public function msg ( $text, $type = "info", $buttons = array() ) {
		$buttons = $this->parse_params( $buttons );
		switch ( $type ) {
			case "ok":
				$type = "success";
			break;
			case "notice":
				$type = "info";
			break;
			case "err":
			case "error":
				$type = "danger";
			break;
			case "alert":
				$type = "warning";
			break;
			default:
				$type = "info";
			break;
		}

		if ( is_array( $buttons ) ) {
			$i = 0;
			foreach ( $buttons as $button ) {
				//switch ($){}
				$btn_class = "";
				$button_output = '<button type="button" data-link="'.$button["link"].'" class="btn btn-default btn-'.$btn_class.'">'.$button["text"].'</button>'; 
				$buttons[$i] = $button_output;
				$i++;
			}
		} else { $buttons = array(); }
		$type = htmlspecialchars( $type );
		return '<div class="bs-component">
			<div class="message alert alert-dismissable alert-'.$type.'">'.$text.(count($buttons) ? '<hr>' : '').implode("&nbsp;&nbsp;", $buttons).'</div>
		</div>';
	}
	////////////////////////////////////////////////////////////////////
	public function get_domain_by_id ( $id ) {
		$data = $this->core->db->select( "*" )
			->from( "domains" )
			->where("id:{$id}")
			->order("date_visited")
			->limit(1)
			->rows();
		if (count($data)){
			return $data[0]["domain"];
		}
		return "---err---";
	}
	////////////////////////////////////////////////////////////////////
	public function get ( $var ) {
		return (isset($_GET[$var]) ? htmlspecialchars($_GET[$var]) : false);
	}
	////////////////////////////////////////////////////////////////////
	public function post ( $var, $dontescape = false ) {
		if ( $dontescape == true ) {
			return (isset($_POST[$var]) ? $_POST[$var] : false);
		}
		if ( isset( $_POST[ $var ] ) && is_array( $_POST[ $var ] ) ) {
			$out = array();
			foreach ( $_POST[$var] as $key => $val ) {
				$out[htmlspecialchars($key)] = htmlspecialchars($val);
			}
			return $out;
		} else {
			return (isset($_POST[$var]) ? htmlspecialchars($_POST[$var]) : false);
		}
	}
	////////////////////////////////////////////////////////////////////
	public function debug ( $data ) {
		if (is_array($data) || is_object($data)){
			$this->dbg[] = "[".date("H:m:s")."]: ".var_export($data, true);
			
		}else{
			$this->dbg[] = "[".date("H:m:s")."]: ".$data;
		}
	}
	////////////////////////////////////////////////////////////////////
	public function striphtml ( $text ) {
		return strip_tags( $text );
	}
	////////////////////////////////////////////////////////////////////
	public function format_date ( $date, $config = false) {
		$months = Array(
			1 => "Ledna", 
			2 => "Února", 
			3 => "Března", 
			4 => "Dubna", 
			5 => "Května", 
			6 => "Června", 
			7 => "Července", 
			8 => "Srpna", 
			9 => "Září", 
			10 => "Října", 
			11 => "Listopadu", 
			12 => "Prosince"
		);
		if ( $parts = preg_split( "/-|\s|:/", $date ) ) {
			$out = "";
			if (isset($parts[1])){
				if ( substr( $parts[1], 0, 1 ) == "0" ) {
					$month = substr( $parts[1], 1, 1 );
				} else {
					$month = $parts[1];
				}
			}
			if ( $config == "DATE_ONLY" ) {
				return $parts[2].".&nbsp;".$months[$month]." ".$parts[0];
			} else if ( $config == "TIME_ONLY" ) {
				return $parts[3].":".$parts[4].":".$parts[5];
			}
			if (!empty($parts[2]) && !empty($months[$month]) && !empty($parts[0])){
				$out = $parts[2].".&nbsp;".$months[$month]." ".$parts[0];
			}
			if (!empty($parts[3]) && !empty($parts[4]) && !empty($parts[5])) {
				$out.= " | ".$parts[3].":".$parts[4].":".$parts[5];
			}
			return $out;
		} else {
			return "---";
		}
	}
	////////////////////////////////////////////////////////////////////
	public function strlen ($str){
		return strlen($str);
	}
	////////////////////////////////////////////////////////////////////
	public function normalize_file_name ($name, $except = false) {
		$name = mb_strtolower($name, "UTF-8");
		$replace = array(
			"ť" => "t", "ž" => "z", "š" => "s", "!" => "", "\"" => "", "#" => "", 
			"$" => "", "%" => "", "&" => "", "'" => "", "(" => "", ")" => "", 
			"*" => "", "+" => "-", "," => "", "-" => "-", "." => "", "/" => "", 
			":" => "", ";" => "", "<" => "", "=" => "", ">" => "", "?" => "", 
			"@" => "", "[" => "", "\\" => "", "]" => "", "^" => "",// "_" => "", 
			"`" => "", "{" => "", "|" => "", "~" => "", "€" => "euro", "„" => "", 
			"…" => "", "†" => "", "‡" => "", "‰" => "", "‹" => "", "ś" => "s", 
			"ź" => "z", "‘" => "", "’" => "", "“" => "", "”" => "", "•" => "", 
			"–" => "-", "—" => "-", "™" => "", "›" => "", " " => "", "˘" => "", 
			"Ł" => "", "¤" => "", "ą" => "a", "¦" => "", "§" => "", "¨" => "", 
			"©" => "", "ş" => "s", "«" => "", "¬" => "", "®" => "", "ż" => "z", 
			"°" => "", "±" => "", "˛" => "", "ł" => "", "´" => "", "µ" => "", 
			"¶" => "", "·" => "", "¸" => "", "»" => "", "ľ" => "l", "˝" => "", 
			"ŕ" => "r", "â" => "a", "ă" => "a", "ä" => "a", "ĺ" => "l", "ć" => "c", 
			"ç" => "c", "ę" => "e", "ë" => "e", "ě" => "e", "í" => "i", "î" => "i", 
			"ď" => "d", "đ" => "d", "ń" => "n", "ň" => "n", "ó" => "o", "ô" => "o", 
			"ő" => "o", "ö" => "o", "×" => "", "ř" => "r", "ů" => "u", "ú" => "u", 
			"ű" => "u", "ü" => "u", "ý" => "y", "ţ" => "t", "ŕ" => "r", "ß" => "", 
			"á" => "a", "â" => "a", "ă" => "a", "ä" => "a", "ĺ" => "l", "ć" => "c", 
			"ç" => "c", "č" => "c", "é" => "e", "ę" => "e", "ë" => "e", "ě" => "e", 
			"í" => "i", "î" => "i", "ď" => "d", "đ" => "d", "ń" => "n", "ň" => "n", 
			"ó" => "o", "ô" => "o", "ő" => "o", "ö" => "o", "÷" => "", "ř" => "r", 
			"ů" => "u", "ú" => "u", "ű" => "u", "ü" => "u", "ý" => "y", "ţ" => "t", 
			"˙" => "", " " => "-", 
		);
		$replace_from = array();
		$replace_to = array();
		foreach ($replace as $k => $v) {
			if ($except == false || $except != $k) {
				$replace_from[] = $k;
				$replace_to[] = $v;
			}
		}
		$name = str_replace($replace_from, $replace_to, $name);
		return $name;
	}
	////////////////////////////////////////////////////////////////////
	public function urlize ($str){
		$str = mb_strtolower($str, "UTF-8");
		$replace = array(
			"ť" => "t", "ž" => "z", "š" => "s", "!" => "", "\"" => "", "#" => "", 
			"$" => "", "%" => "", "&" => "", "'" => "", "(" => "", ")" => "", 
			"*" => "", "+" => "-", "," => "", "-" => "-", "." => "", "/" => "", 
			":" => "", ";" => "", "<" => "", "=" => "", ">" => "", "?" => "", 
			"@" => "", "[" => "", "\\" => "", "]" => "", "^" => "",// "_" => "", 
			"`" => "", "{" => "", "|" => "", "~" => "", "€" => "euro", "„" => "", 
			"…" => "", "†" => "", "‡" => "", "‰" => "", "‹" => "", "ś" => "s", 
			"ź" => "z", "‘" => "", "’" => "", "“" => "", "”" => "", "•" => "", 
			"–" => "-", "—" => "-", "™" => "", "›" => "", " " => "", "˘" => "", 
			"Ł" => "", "¤" => "", "ą" => "a", "¦" => "", "§" => "", "¨" => "", 
			"©" => "", "ş" => "s", "«" => "", "¬" => "", "®" => "", "ż" => "z", 
			"°" => "", "±" => "", "˛" => "", "ł" => "", "´" => "", "µ" => "", 
			"¶" => "", "·" => "", "¸" => "", "»" => "", "ľ" => "l", "˝" => "", 
			"ŕ" => "r", "â" => "a", "ă" => "a", "ä" => "a", "ĺ" => "l", "ć" => "c", 
			"ç" => "c", "ę" => "e", "ë" => "e", "ě" => "e", "í" => "i", "î" => "i", 
			"ď" => "d", "đ" => "d", "ń" => "n", "ň" => "n", "ó" => "o", "ô" => "o", 
			"ő" => "o", "ö" => "o", "×" => "", "ř" => "r", "ů" => "u", "ú" => "u", 
			"ű" => "u", "ü" => "u", "ý" => "y", "ţ" => "t", "ŕ" => "r", "ß" => "", 
			"á" => "a", "â" => "a", "ă" => "a", "ä" => "a", "ĺ" => "l", "ć" => "c", 
			"ç" => "c", "č" => "c", "é" => "e", "ę" => "e", "ë" => "e", "ě" => "e", 
			"í" => "i", "î" => "i", "ď" => "d", "đ" => "d", "ń" => "n", "ň" => "n", 
			"ó" => "o", "ô" => "o", "ő" => "o", "ö" => "o", "÷" => "", "ř" => "r", 
			"ů" => "u", "ú" => "u", "ű" => "u", "ü" => "u", "ý" => "y", "ţ" => "t", 
			"˙" => "", " " => "-", 
		);
		$replace_from = array();
		$replace_to = array();
		foreach ($replace as $k => $v) {
			$replace_from[] = $k;
			$replace_to[] = $v;
		}
		$str = str_replace($replace_from, $replace_to, $str);
		return $str;
	}
	////////////////////////////////////////////////////////////////////
	public function minimizeHTML ( $data ) {
		// removes CR LF & tab
		$replace = array(
			"\r" => "", 
			"\n" => "", 
			"\t" => "" 
		);
		$data = str_replace( array_keys( $replace ), array_values( $replace ), $data );
		// removes multiple double spaces
		while ( preg_match( "/\s{2}/", $data ) ) {
			$data = str_replace( "  ", " ", $data );
		}
		return $data;
	}
	////////////////////////////////////////////////////////////////////
	public function getDebug ( ) {
		if ( is_array( $this->dbg ) ) {
			return implode("<br>", $this->dbg);	
		}
		return "no debug data";
	}
	////////////////////////////////////////////////////////////////////
	public function var_dump($var){
		return "var_dump: ".var_export($var, true);
	}
	////////////////////////////////////////////////////////////////////
	public function parse_params ( $params ) {
		if ( $params == false ) { return $params; }
		if ( is_array( $params ) ) { return $params; }
		$out = array(); 
		if ( strlen( $params ) && preg_match( "/,/", $params ) ) { 
			$replace = array( "\r" => "", "\n" => "", "\t" => "", " " => "" ); 
			$params = str_replace( array_keys( $replace ), array_values( $replace ), $params );
			$split = explode( ",", $params ); 
			foreach ( $split as $param ) { 
				if ( preg_match( "/:/", $param ) ) { 
					$split2 = explode( ":", $param ); 
					$out[ $split2[ 0 ] ] = $split2[ 1 ]; 
				} else if ( preg_match( "/=/", $params ) ) { 
					$split2 = explode( "=", $param ); 
					$out[ $split2[ 0 ] ] = $split2[ 1 ]; 
				} 
			} 
			return $out; 
		} else { 
			if ( preg_match( "/:/", $params ) ) { 
				$split = explode( ":", $params ); 
				return array( $split[ 0 ] => $split[ 1 ] ); 
			} else if ( preg_match( "/=/", $params ) ) { 
				$split = explode( "=", $params ); 
				return array( $split[ 0 ] => $split[ 1 ] ); 
			} else if ( is_int( $params ) ) {
				return $params;
			} else if ( is_string( $params ) ) {
				return $params;
			}
		} 
		return ""; 
	}
	////////////////////////////////////////////////////////////////////
	public function getVariables () {
		$out = array();
		$out["debug"] = array();
		if (is_array($this->dbg) && count($this->dbg)){
			$out["debug"] = implode("<br>", $this->dbg);
		}
		$out["config"] = array();
		if (isset($this->core->config)){
			foreach ( $this->core->config as $key => $val ) {
				$out["config"][htmlspecialchars($key)] = htmlspecialchars($val);
			}
		}
		$out["get"] = array();
		foreach ( $_GET as $key => $val ) {
			if (!is_array($val) && !is_object($val)){
				$out["get"][htmlspecialchars($key)] = htmlspecialchars($val);
			}
		}
		$out["post"] = array();
		foreach ( $_POST as $key => $val ) {
			if (!is_array($val) && !is_object($val)){
				$out["post"][htmlspecialchars($key)] = htmlspecialchars($val);
			}
		}
		$out["url"] = array();
		$this->url = "http://".$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
		$this->url = str_replace($this->core->config["base_url"], "", $this->url);
		$segments = explode( '/', $this->url );
		$out["url"]["full"] = $this->url;
		foreach ( $segments as $item ) {
			$out["url"]["parts"][] = $item;
		}
		$out["session"] = array();
		if (isset($_SESSION)){
			foreach ( $_SESSION as $key => $val ) {
				$out["session"][htmlspecialchars($key)] = htmlspecialchars($val);
			}
		}
		return $out;
	}
	////////////////////////////////////////////////////////////////////
	public function format_scripts ($scripts = array()){
		if (is_array($scripts) && count($scripts)){
			$out = array();
			$len = count($scripts);
			foreach ($scripts as $script){
				$out[] =',{ '.$script["name"].': "'.$script["path"].'" }';
			}
			return implode("\n", $out);
		}
		return "";
	}
	////////////////////////////////////////////////////////////////////
	public function getTimeUsage () {
		if ($this->timer_start && $this->timer_end) {
			$out = ($this->timer_end - $this->timer_start);
			return substr($out, 0, 6)." secs";
		}
		return 0;
	}
	
	////////////////////////////////////////////////////////////////////
	public function setTimer ($type) {
		switch ($type) {
			case "start":
				List ($usec, $sec) = Explode (' ', microtime());
				$this->timer_start = ((float)$sec + (float)$usec);
			break;
			case "end":
				List ($usec, $sec) = Explode (' ', microtime());
				$this->timer_end = ((float)$sec + (float)$usec);
			break;
		}
	}
	////////////////////////////////////////////////////////////////////
	public function hash ( $str, $salt = "56456d4fg4654dfg" ) {
		return md5( md5( $str.$salt ) );
	}
	////////////////////////////////////////////////////////////////////
	public function getMemoryUsage (){
		return (int)(memory_get_usage(true) / 1048576)." MB";
	}
	////////////////////////////////////////////////////////////////////
	
}
?>