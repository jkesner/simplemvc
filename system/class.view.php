<?php

class View {
	private $dwoo;
	private $dwoo_data;

	private $pageVars = array();
	private $template;

	////////////////////////////////////////////////////////////////////
	public function __construct( &$core ) {
		$this->core = $core;
		$this->core->tools->debug("View initialized");
		include_once( ROOT_DIR.'system/dwoo/dwooAutoload.php' ); 
		$this->dwoo = new Dwoo();
		$this->dwoo_data  = new Dwoo_Data();
		$this->compiler = new Dwoo_Compiler();
	
	}
	////////////////////////////////////////////////////////////////////
	public function load ( $file ) {
		$this->core->tools->debug("View->load()");
		$file = APP_DIR."views/".$file;
		if ( file_exists( $file ) ) {
			include_once( ROOT_DIR.'system/dwoo/dwooAutoload.php' ); 
			return new Dwoo_Template_File( $file ); 
		} else {
			print ( "template {$file} doesnt exist" );
			return false;
		}
	}
	////////////////////////////////////////////////////////////////////
	public function render ( $file, $data ) {
		$this->core->tools->debug("View->render()");
		$file = $this->load( $file );
		$data["core"] = $this->core->tools->getVariables();
		$this->dwoo->addPlugin( "load", array( $this, "call" ) );
		foreach ( $data as $key => $val ) {
			$this->dwoo_data->assign( $key, $val );
		}
		$out = $this->dwoo->get( $file, $this->dwoo_data );
		$out = $this->core->tools->minimizeHTML($out);
		return $out;
	}
	////////////////////////////////////////////////////////////////////
	/* render alias */
	public function get ( $file, $data ) { 
		return $this->render( $file, $data ); 
	}
	////////////////////////////////////////////////////////////////////
	public function call ( $object, $method, $p1 = "", $p2 = "", $p3 = "", $p4 = "" ) {
		$this->core->tools->debug("View->call()");
		if ( isset($this->core->$object) && is_object( $this->core->$object ) && method_exists( $this->core->$object, $method ) ) {
			return $this->core->$object->$method( $p1, $p2, $p3, $p4 );
		}
		return "dwoo->plugin->call({$object}->{$method}()) error";
	}
	////////////////////////////////////////////////////////////////////
}

?>