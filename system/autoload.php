<?php
/* autload function 
 * @param string $fileName - name of class to load
 */
function __autoload( $fileName ) {
	$fileName = strtolower( $fileName );
	if ( !class_exists( $fileName, false ) || !interface_exists( $fileName, false ) ) {
		if ( file_exists( ROOT_DIR."system/class.{$fileName}.php" ) ) {
			require_once ( ROOT_DIR."system/class.{$fileName}.php" );
}	}	}
?>