<?php

class Controller {
	
	public function __construct ( &$core ) {
		$this->core = $core;
	}
	
	public function loadModel( $name )
	{
		$model = false;
		$this->core->tools->debug( "Controller->loadModel()" );
		try {
			if (!class_exists($name)){
				require( APP_DIR .'models/'. $name .'.php' );
			}
			$model = new $name( $this->core );
		} catch( FileException $e ) {
			$this->core->tools->debug( "Controller->loadModel() exception - class not exist" );
			throw new FileException( "Error class {$name} not exist: ".$e->getMessage(), 1 );
		}
		return $model;
	}
	
	public function loadView()
	{
		$this->core->tools->debug("Controller->loadView()");
		$view = false;
		if ( class_exists( "View" ) ) {
			$view = new View( $this->core );
		} else {
			$this->core->tools->debug( "Controller->loadModel() exception " );
			throw new FileException( "Controller->loadModel() exception class View not exist", 1 );
		}
		return $view;
	}
	
	public function loadPlugin( $name )
	{
		$this->core->tools->debug( "Controller->loadPlugin()" );
		try {
			if ( file_exists( APP_DIR .'plugins/'. strtolower( $name ) .'.php' ) ) {
				require( APP_DIR .'plugins/'. strtolower( $name ) .'.php');
			} 
		} catch( FileException $e ) {
			throw new FileException( $e->getMessage(), 1 );
		}
	}
	
	public function loadHelper( $name )
	{
		$this->core->tools->debug( "Controller->loadHelper()" );
		$helper = false;
		try {
			if ( file_exists( APP_DIR .'helpers/'. strtolower( $name ) .'.php' ) ) {
				require( APP_DIR .'helpers/'. strtolower( $name ) .'.php' );
				$helper = new $name( $this->core );
			} 
		} catch( FileException $e ) {
			throw new FileException( $e->getMessage(), 1 );
		}
		return $helper;
	}
	
	public function redirect( $loc )
	{
		header( 'Location: '. $this->core->config['base_url'] . $loc );
		exit;
	}
    
}

?>