<?php
class core {
	public $url = "";
	public $controller = "";
	public $action = "";
	public $config = "";
	///////////////////////////////////////////////////////////////////////////
	public function __construct ( ) {
		if ( !APP_DIR || !ROOT_DIR || !file_exists( APP_DIR . "config/config.php" ) ) { 
			die( "fatal system error" ); 
		}
		require( APP_DIR .'config/config.php' );
		$this->config = $config;
		if ( empty( $this->config["base_url"] ) ) { 
			$this->config["base_url"] = "http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/";
		}
		$this->error = new exception( );
		$this->tools = new tools( $this );
		$this->tools->setTimer( "start" );
		$this->tools->debug( "Core initializing" );
		$this->db = new DB( );
		if (isset($this->config["db_username"]) && !empty($this->config["db_username"])){
			DB::$user = $this->config["db_username"];
		}else{
			throw new error("Error db_username not set", 1);
		}
		if (isset($this->config["db_username"])){
			DB::$password = $this->config["db_password"];
		}
		if (isset($this->config["db_name"])){
			DB::$dbName = $this->config["db_name"];
		}else{
			throw new error("Error db_name not set", 1);
		}
		$this->model = new Model( $this );
		$this->controller = new Controller( $this );
		$this->view = new View( $this );
		define( 'BASE_URL', $this->config['base_url'] );
		$this->controller = $this->config['default_controller'];
	    $this->action = 'index';
	}

	///////////////////////////////////////////////////////////////////////////
	public function route (){
		$this->tools->debug("Core->route() called");
		$this->url = "http://".$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
		$this->url = str_replace( $this->config["base_url"], "", $this->url );

		$segments = explode( '/', $this->url );
		
		if( isset( $segments[0] ) && $segments[0] != '' ) {
			$this->controller = $segments[0];
		}
		if( isset( $segments[1] ) && $segments[1] != '' ) {
			$this->action = $segments[1];
		}

		require_once( ROOT_DIR."system/class.exception.php" );
		
		require_once( APP_DIR."controllers/main.php" );
		$this->main = new Main( $this );
		
		require_once( APP_DIR."controllers/subpage.php" );
		$this->main = new Main( $this );


		$this->tools->debug("Core->route(".$this->url."): controller: {$this->controller} | action: {$this->action}");
		$path = APP_DIR . 'controllers/' . $this->controller . '.php';
		if( file_exists( $path ) ) {
		    require_once( $path );
		} else {
			/* subpage */
			$path = APP_DIR . 'controllers/subpage.php';
	        if ( file_exists( $path ) ) { 
	        	require_once( $path ); 
	        	$this->tools->debug("loading subpage controller"); 
	        	$this->controller = "subpage";
	        	$this->action = "handler"; 
	        } 
	        $obj = new $this->controller( $this );
			$this->tools->setTimer( "end" );
			die( 
		    	call_user_func_array( 
			    	array( $obj, $this->action ), 
			    	$segments 
		    	)
		    );
		}
	    
	    if( !method_exists( $this->controller, $this->action ) ) {
	        print "method doesnt exists: {$this->controller}/{$this->action}";
	        $this->controller = $this->config['error_controller'];
	        require_once( APP_DIR . 'controllers/' . $this->controller . '.php' );
	        $this->action = 'index';
	    }
		$obj = new $this->controller( $this );
		$action = $this->action;
		$this->tools->setTimer( "end" );
		die ( 
	    	call_user_func_array( 
		    	array( $obj, $this->action ), 
		    	array_slice( $segments, 2 ) 
	    	)
	    );
	}
}

?>
