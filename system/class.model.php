<?php

class Model {

	public $connection;

	public function __construct ( &$core ) {
		$this->core = $core;
		$this->core->tools->debug("Model initialized");
		
		
	}

	public function escapeString($string)
	{
		$this->core->tools->debug("Model->escapeString()");
		return mysql_real_escape_string($string);
	}

	public function escapeArray($array)
	{
		$this->core->tools->debug("Model->escapeArray()");
	    array_walk_recursive($array, create_function('&$v', '$v = mysql_real_escape_string($v);'));
		return $array;
	}
	
	public function to_bool($val)
	{
		$this->core->tools->debug("Model->to_bool()");
	    return !!$val;
	}
	
	public function to_date($val)
	{
	    return date('Y-m-d', $val);
	}
	
	public function to_time($val)
	{
	    return date('H:i:s', $val);
	}
	
	public function to_datetime($val)
	{
	    return date('Y-m-d H:i:s', $val);
	}
	
	public function query($qry)
	{
		$result = mysql_query($qry) or die('MySQL Error: '. mysql_error());
		$resultObjects = array();

		while($row = mysql_fetch_object($result)) $resultObjects[] = $row;

		return $resultObjects;
	}

	public function execute($qry)
	{
		$exec = mysql_query($qry) or die('MySQL Error: '. mysql_error());
		return $exec;
	}
    
}
?>
