<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><section class="content services container container_12 t_container_8 m_container_4 center">
	<div class="editorContent grid_8 t_grid_5 m_grid_4">
		<?php if ((isset($this->scope["homepage"]["0"]) ? $this->scope["homepage"]["0"]:null) != "" && (isset($this->scope["homepage"]["0"]["sekce_obsah"]) ? $this->scope["homepage"]["0"]["sekce_obsah"]:null)) {
?>
			<?php echo $this->scope["homepage"]["0"]["sekce_obsah"];?>

		<?php 
}
else {
?>
			<a href="?install=1" class="btn" title="">install &raquo;</a>
		<?php 
}?>

	</div>
	<div class="sidebar news grid_4 t_grid_3 m_grid_4">
		<div class="padd">
			<h3>News</h3>
			<ul class="newsList">
				<?php 
$_fh0_data = (isset($this->scope["news"]) ? $this->scope["news"] : null);
if ($this->isArray($_fh0_data) === true)
{
	foreach ($_fh0_data as $this->scope['n'])
	{
/* -- foreach start output */
?>
					<li>
						<p>
							<strong><?php echo $this->scope["n"]["novinky_nadpis"];?></strong><br>
							<time datetime="<?php echo $this->scope["n"]["novinky_datum"];?>">
								<?php echo call_user_func(array($this->plugins['load']['callback'][0], 'call'), 'tools', 'format_date', (isset($this->scope["n"]["novinky_datum"]) ? $this->scope["n"]["novinky_datum"]:null), 'DATE_ONLY', '', '');?>

							</time><br>
							<?php echo call_user_func(array($this->plugins['load']['callback'][0], 'call'), 'tools', 'striphtml', (isset($this->scope["n"]["novinky_text"]) ? $this->scope["n"]["novinky_text"]:null), '', '', '');?>

						</p>
					</li>
				<?php 
/* -- foreach end output */
	}
}?>

			</ul>
		</div>
	</div>
</section><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>