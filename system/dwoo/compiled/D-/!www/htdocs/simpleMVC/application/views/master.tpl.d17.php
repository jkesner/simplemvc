<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><!DOCTYPE html>
<html lang="cs">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $this->scope["title"];?></title>
		<base href="<?php echo $this->scope["core"]["config"]["base_url"];?>">
		<link href="static/css/style.less" type="text/less" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="static/js/head.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript">
			head.js( 
		        { less: "static/js/less-1.4.1.min.js" }, 
		        { jQuery: "static/js/jquery-1.11.3.min.js" }, 
		        { bootStrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" }
			);
		</script>
	</head>
	<body>
		<header class="container container_12 t_container_8 m_container_4 center">
			<nav class="navbar navbar-default main grid_12 t_grid_8 m_grid_4">
				<div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		              	<span class="sr-only">Toggle navigation</span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="<?php echo $this->scope["core"]["config"]["base_url"];?>#">SimpleMVC</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
	            	<ul class="nav navbar-nav">
						<?php 
$_fh2_data = (isset($this->scope["nav"]) ? $this->scope["nav"] : null);
if ($this->isArray($_fh2_data) === true)
{
	foreach ($_fh2_data as $this->scope['item'])
	{
/* -- foreach start output */
?>
							<li class="level_1">
								<?php if ((isset($this->scope["item"]["submenu"]) ? $this->scope["item"]["submenu"]:null) != false && (isset($this->scope["item"]["hideSubmenu"]) ? $this->scope["item"]["hideSubmenu"]:null) != 1) {
?>
									<a href="#" title="<?php echo $this->scope["item"]["name"];?>" class="dropdown-toggle" data-toggle="dropdown" role="button" 
										aria-haspopup="true" aria-expanded="false">
										<?php echo $this->scope["item"]["name"];?> <span class="caret"></span>
									</a>
									<ul class="dropdown-menu submenu level_2">
										<?php 
$_fh1_data = (isset($this->scope["item"]["submenu"]) ? $this->scope["item"]["submenu"]:null);
if ($this->isArray($_fh1_data) === true)
{
	foreach ($_fh1_data as $this->scope['sm'])
	{
/* -- foreach start output */
?>
											<li class="level_2 fourth">
												<a href="<?php echo $this->scope["sm"]["url"];?>" title="<?php echo $this->scope["sm"]["name"];?> - <?php echo $this->scope["item"]["name"];?>" class="level_2">
													<?php echo $this->scope["sm"]["name"];?>

												</a>
											</li>
										<?php 
/* -- foreach end output */
	}
}?>

									</ul>
								<?php 
}
else {
?>
									<a href="<?php echo $this->scope["item"]["url"];?>" title="<?php echo $this->scope["item"]["name"];?>"><?php echo $this->scope["item"]["name"];?></a>
								<?php 
}?>

							</li>
						<?php 
/* -- foreach end output */
	}
}?>

					</ul>
				</div>
			</nav>
		</header>
		
		<?php echo $this->scope["content"];?>

		
		<footer class="container container_12 t_container_8 m_container_4 center">
			<div class="grid_6 t_grid_4 m_grid_4">
				simpleMVC is responsive recoded fork of <a href="https://github.com/gilbitron/PIP" target="_blank" title="fork of pip">pip</a>,<br> 
				uses: <a href="http://dwoo.org" target="_blank" title="DWOO template engine">DWOO template engine</a>, <a href="http://lesscss.org/" target="_blank">LESSCSS</a>, <a href="http://jquery.com" target="_blank">jQuery</a>,  
				<a href="http://getbootstrap.com/" target="_blank" title="bootstrap v3">bootstrap v3</a> & <a href="http://960.gs/" target="_blank" title="960.gs">960 grid system</a>
			</div>
			<div class="grid_6 t_grid_4 m_grid_4">
				Memory usage: <?php echo call_user_func(array($this->plugins['load']['callback'][0], 'call'), 'tools', 'getMemoryUsage', '', '', '', '');?><br>
				Process time: <?php echo call_user_func(array($this->plugins['load']['callback'][0], 'call'), 'tools', 'getTimeUsage', '', '', '', '');?>

			</div>
		</footer>

		<div class="debug container container_12 t_container_8 m_container_4 center">
			<div class="grid_12 t_grid_8 m_grid_4">
				<strong>Debug:</strong><br>
				<?php echo $this->scope["core"]["debug"];?>

			</div>
		</div>
	</body>
</html><?php  /* end template body */
return $this->buffer . ob_get_clean();
?>