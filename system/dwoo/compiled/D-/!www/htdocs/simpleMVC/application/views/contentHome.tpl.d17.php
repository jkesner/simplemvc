<?php
/* template head */
/* end template head */ ob_start(); /* template body */ ?><div class="grid_12 t_grid_8 m_grid_4">
	<div class="jumbotron">
		<h1>SimpleMVC showcase</h1>
	  	<p>
	  		this is showcase of simple PHP MVC application, responsive recoded fork of <a href="https://github.com/gilbitron/PIP" target="_blank" title="fork of pip">pip</a>,<br> 
				uses: <a href="http://dwoo.org" target="_blank" title="DWOO template engine">DWOO template engine</a>, <a href="http://lesscss.org/" target="_blank">LESSCSS</a>, <a href="http://jquery.com" target="_blank">jQuery</a>,  
				<a href="http://getbootstrap.com/" target="_blank" title="bootstrap v3">bootstrap v3</a> & <a href="http://960.gs/" target="_blank" title="960.gs">960 grid system</a>

	  	</p>
	  	<p>
	    	<a class="btn btn-lg btn-primary" href="#" role="button">Lorem ipsum »</a>
	  	</p>
	</div>
</div>
<div class="grid_12 t_grid_8 m_grid_4">
	<?php echo $this->scope["content"];?>

</div>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>